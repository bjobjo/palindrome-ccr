#include "Key.h"

Key::Key(HW &hw) : m_hw(hw) {}

void Key::press()
{
  if (m_press == KeyPress::no_press) {
    m_first_press_millis = m_hw.get_millis();
    m_press = KeyPress::intermediate_press;
  }
  ++m_count_press;
}

void Key::tick() {
  if (m_press == KeyPress::intermediate_press && (m_first_press_millis + KEY_PRESS_PERIOD_MS) <= m_hw.get_millis()) {
    switch (m_count_press) {
      case 1:
        m_press = KeyPress::single_press;
        break;
      case 2:
        m_press = KeyPress::double_press;
        break;
      default:
        m_press = KeyPress::no_press;
        break;
    }
  }
}

Key::KeyPress Key::detected() {
  if (m_press == KeyPress::intermediate_press) {
    return KeyPress::no_press;
  }

  // we've detected a key press, return it and reset the internal state
  KeyPress current = m_press;
  m_press = KeyPress::no_press;
  m_first_press_millis = 0;
  m_count_press = 0;

  return current;
}

unsigned long Key::count() const
{
  return m_count_press;
}
