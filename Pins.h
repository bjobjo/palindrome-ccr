// vim: ts=2 sw=2 expandtab:

#ifndef PINS_H
#define PINS_H

#include <Arduino.h>

// Where the different functions of the board are located on the CPU.
enum class Pins {
  ledRed = 3,
  ledGreen = 4,
  piezo = 2,
  celln1 = A2,
  celln2 = A1,
  celln3 = A0,
  battn = A3,
  offswitch = 7,
  piezopull = 5,
};

enum class PinMode {
  input = INPUT,
  output = OUTPUT,
};

enum class PinLevel {
  high = HIGH,
  low = LOW,
};

enum class PinTrigger {
  low = LOW,
  change = CHANGE,
  rising = RISING,
  falling = FALLING,
  high = HIGH,
};

#endif // PINS_H
