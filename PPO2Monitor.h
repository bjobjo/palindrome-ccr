#ifndef PPO2MONITOR_H
#define PPO2MONITOR_H

#include "HW.h"

#define LIBCALL_DEEP_SLEEP_SCHEDULER
#include <DeepSleepScheduler.h>

class PPO2Monitor : public Runnable
{
  public:
    PPO2Monitor(HW &hw, double voting_margin = 0.2);

    // the three O2 cells we have available
    enum class O2Cells {
      one = static_cast<int>(Pins::celln1),
      two = static_cast<int>(Pins::celln2),
      three = static_cast<int>(Pins::celln3),
    };

    double voting_margin() const;
    bool cell_failure() const;
    O2Cells which_cell_failed() const;

    double o2cell_average() const;
    double o2cell_millivolt(O2Cells cell) const;
    double o2cell_ppo2(O2Cells cell) const;

    void store_o2_calibration();

    virtual void run() override;

  private:
    /// reference to hardware used for reading ADC values and scheduling this task
    HW &m_hw;
    /// margin of error between cells used when voting
    double m_voting_margin;

    /// do we have any failed cells?
    bool m_cell_failure{false};
    /// the number of consecutive failures for each cell, where a failure is defined to be a reading
    /// outside the margin relative to the other two cells.
    uint8_t m_cell1_fail_count{0}, m_cell2_fail_count{0}, m_cell3_fail_count{0};
    /// which cell had the failure.
    O2Cells m_failed_cell{O2Cells::one};

    /// calibration information for each cell.
    double m_cell1_calibration{0}, m_cell2_calibration{0}, m_cell3_calibration{0};
    /// raw ADC values for each cell.
    double m_cell1_raw{0}, m_cell2_raw{0}, m_cell3_raw{0};
    /// calculated millivolt values for each cell.
    double m_cell1_millivolt{0}, m_cell2_millivolt{0}, m_cell3_millivolt{0};
    /// PPO2 values for each cell
    double m_cell1_ppo2{0}, m_cell2_ppo2{0}, m_cell3_ppo2{0};
    /// the average PPO2 value for all operative cells.
    double m_average_ppo2{0};
};

#endif // PPO2MONITOR_H
