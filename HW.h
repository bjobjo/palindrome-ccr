// vim: ts=2 sw=2 expandtab:

#ifndef HW_H
#define HW_H

#include "Pins.h"

class Runnable;            // forward declare Runnable
class __FlashStringHelper; // forward declare __FlashStringHelper wrapper

typedef void (*isr_handler)(); // function point to interrupt handler

// this is responsible for writing to and reading from the hardware of the
// device, as well as scheduling functions to run immediately and after a delay.
class HW {
  public:
    HW();

    // adc functions
    double adc_read_millivolt(Pins p);

    // pin functions
    void set_pin_mode(Pins p, PinMode mode);
    void digital_write(Pins p, PinLevel level);
    PinLevel digital_read(Pins p);
    void attach_interrupt(Pins p, isr_handler h, PinTrigger trigger);

    // calibration functions
    void read_o2_calibration(double *celln1, double *celln2, double *celln3);
    void store_o2_calibration(double celln1, double celln2, double celln3);
    uint8_t display_orientation();
    void set_display_orientation(uint8_t rot);

    // display functions
    void clear_display();
    void flush_display();
    void flip_display();
    void set_text_size(unsigned int size);
    void set_cursor(int x, int y);
    void print_text(const char *text);
    void print_text(const __FlashStringHelper *text);
    void print_int(unsigned int i);
    void print_double(double n, unsigned char width, unsigned int precision);

    // scheduling functions
    void initialise_task_timeout();
    void schedule_once(Runnable *r);
    void schedule_delay(Runnable *r, unsigned long millis);
    unsigned long get_millis() const;
    void schedule_execute();
    void delay_ms(unsigned long ms);
};

#endif // HW_H
