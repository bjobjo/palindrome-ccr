// vim: ts=2 sw=2 expandtab:

#include "FSM.h"

Context::Context(HW &hw, PPO2Monitor &ppo2) : m_hw(hw), m_ppo2(ppo2) {
}

double Context::vbat() {
  // return vbat reading in volt
  return (m_hw.adc_read_millivolt(Pins::battn) / 1000.0);
}

HW &Context::hw() {
  return m_hw;
}

PPO2Monitor &Context::ppo2() {
  return m_ppo2;
}

LEDHandler::LEDHandler(Context &context) : m_context(context) {}

void LEDHandler::run() {
  // every 5 seconds we want to blink the LEDs according to the following rules:
  // - for a ppO2 of range above 1.6, blink red for each 0.1 above 1.0.
  // - for a ppO2 of a range between 1.0 to 1.6, blink green for each 0.1
  //   above 1.0.
  // - for a ppO2 of range above 1.0 but less than 1.1, blink green of double
  //   length and red once.
  // - for a ppO2 of a range between 0.4 to 1.0, blink yellow for each 0.1
  //   below 1.0.
  // - for a ppO2 of a range below 0.4, blink red for each 0.1 below 1.0.
  // - if a cell has failed, alternate blinking red and green for 500ms before
  //   continuing with the normal blink process with the average of the two
  //   remaining

  switch (m_blink_cycle) {
    case State::read_ppO2: {
        // grab a new ppo2 value, decide how many blinks and of which color we want
        // to perform this cycle.
        double avg = m_context.ppo2().o2cell_average();

        // the deci ppO2 value represents the integer plus the first decimal number
        // of the average ppO2 value, this makes it easier to find the amount of
        // blinks to perform by either adding or subtracting 10 (for a ppO2 of 1.0).
        int deci_pp02 = avg * 10;

        // figure out our next step based on the rules above, note that blink count
        // also counts off states.
        if (deci_pp02 >= 16) {
          m_next_blink_count = deci_pp02 - 10;
          m_next_blink_count *= 2;
          m_next_cycle = State::blink_red;
        } else if (deci_pp02 >= 11 && deci_pp02 < 16) {
          m_next_blink_count = deci_pp02 - 10;
          m_next_blink_count *= 2;
          m_next_cycle = State::blink_green;
        } else if (deci_pp02 >= 10 && deci_pp02 < 11) {
          m_next_blink_count = 2;
          m_next_cycle = State::blink_alternate;
        } else if (deci_pp02 >= 4 && deci_pp02 < 10) {
          m_next_blink_count = 10 - deci_pp02;
          m_next_blink_count *= 2;
          m_next_cycle = State::blink_yellow;
        } else {
          // ppO2 below 0.4
          m_next_blink_count = 10 - deci_pp02;
          m_next_blink_count *= 2;
          m_next_cycle = State::blink_red;
        }

        // did we get any cell failures?
        if (m_context.ppo2().cell_failure()) {
          m_blink_cycle = State::cell_failure;
          m_blink_count = 10;
        } else {
          // no cell failures, go directly to the next state
          m_blink_cycle = m_next_cycle;
          m_blink_count = m_next_blink_count;
        }

        // immediately schedule the next run.
        m_context.hw().schedule_once(this);
      } break;
    case State::cell_failure:
      // toggle green and red LEDs with a frequency of 20Hz until we've completed
      // m_blink_count runs, after this go to the next state.
      if (m_blink_count > 0) {
        // alternate between toggling the red and green LEDs, and schedule further
        // runs of this code after 50ms delay (for 20Hz refresh rate)
        if (m_blink_count % 2 == 0) {
          m_context.hw().digital_write(Pins::ledRed, PinLevel::low);
          m_context.hw().digital_write(Pins::ledGreen, PinLevel::high);
        } else {
          m_context.hw().digital_write(Pins::ledGreen, PinLevel::low);
          m_context.hw().digital_write(Pins::ledRed, PinLevel::high);
        }
        --m_blink_count;
        m_context.hw().schedule_delay(this, 50);
      } else {
        // turn off both LEDs
        m_context.hw().digital_write(Pins::ledRed, PinLevel::high);
        m_context.hw().digital_write(Pins::ledGreen, PinLevel::high);

        // wait for 300ms prior to going to the next state
        m_blink_cycle = m_next_cycle;
        m_blink_count = m_next_blink_count;
        m_context.hw().schedule_delay(this, 300);
      }
      break;
    case State::blink_alternate:
      // blink green LED twice as long as the RED led
      if (m_blink_count > 0) {
        if (m_blink_count % 2 == 0) {
          m_context.hw().digital_write(Pins::ledGreen, PinLevel::low);
          m_context.hw().digital_write(Pins::ledRed, PinLevel::high);
          m_context.hw().schedule_delay(this, 200);
        } else {
          m_context.hw().digital_write(Pins::ledRed, PinLevel::low);
          m_context.hw().digital_write(Pins::ledGreen, PinLevel::high);
          m_context.hw().schedule_delay(this, 100);
        }
        --m_blink_count;
      } else {
        // we've done all of the blinks we can be expected to do, schedule the
        // next read.
        m_blink_cycle = State::delay_until_next_read;
        m_context.hw().schedule_once(this);
      }
      break;
    case State::blink_yellow:
      // we simulate yellow by lighting both LEDs at once.
      // toggle the given LED with a frequency of 10Hz
      if (m_blink_count > 0) {
        if (m_blink_count % 2 == 0) {
          m_context.hw().digital_write(Pins::ledRed, PinLevel::low);
          m_context.hw().digital_write(Pins::ledGreen, PinLevel::low);
        } else {
          m_context.hw().digital_write(Pins::ledGreen, PinLevel::high);
          m_context.hw().digital_write(Pins::ledRed, PinLevel::high);
        }

        // decrement the blink count and schedule the next blink.
        --m_blink_count;
        m_context.hw().schedule_delay(this, 100);
      } else {
        // we've done all of the blinks we can be expected to do, schedule the
        // next read.
        m_blink_cycle = State::delay_until_next_read;
        m_context.hw().schedule_once(this);
      }
      break;
    case State::blink_red:
      led_toggle(Pins::ledRed);
      break;
    case State::blink_green:
      led_toggle(Pins::ledGreen);
      break;
    case State::delay_until_next_read:
      // make sure both LEDs are off
      m_context.hw().digital_write(Pins::ledRed, PinLevel::high);
      m_context.hw().digital_write(Pins::ledGreen, PinLevel::high);

      // we want to perform this blinking every 2 seconds
      m_blink_cycle = State::read_ppO2;
      m_context.hw().schedule_delay(this, 2000);
      break;
  }
}

LEDHandler::State LEDHandler::blink_cycle() const
{
  return m_blink_cycle;
}

void LEDHandler::led_toggle(Pins p) {
  // toggle the given LED with a frequency of ~3Hz
  if (m_blink_count > 0) {
    if (m_blink_count % 2 == 0) {
      m_context.hw().digital_write(p, PinLevel::low);
    } else {
      m_context.hw().digital_write(p, PinLevel::high);
    }

    // decrement the blink count and schedule the next blink.
    --m_blink_count;
    m_context.hw().schedule_delay(this, 300);
  } else {
    // we've done all of the blinks we can be expected to do, schedule the next
    // read.
    m_blink_cycle = State::delay_until_next_read;
    m_context.hw().schedule_once(this);
  }
}

FSM::FSM(HW &hw, PPO2Monitor &ppo2, Key &key, FSM::States initial)
  : m_state(initial), m_context(hw, ppo2), m_key(key), m_led(m_context) {
  // immediately schedule a run of ourselves and the LED handling
  m_context.hw().schedule_once(this);
  m_context.hw().schedule_once(&m_led);
}

void FSM::run() {
  // prepare the run the states by ticking the key handler and clearing the screen
  m_key.tick();
  m_context.hw().clear_display();

  // handle the current state
  switch (m_state) {
    case States::calibration:
      // if we get a double key press during calibration,
      // we shall store the result and transition to the
      // dive average state
      if (m_key.detected() == Key::KeyPress::double_press) {
        m_context.ppo2().store_o2_calibration();
        transition(States::dive_average);
      }
      // otherwise, just draw the calibration information
      else {
        draw_calibration();
      }
      break;
    case States::dive_average:
      // draw the dive average screen and handle the menu
      handle_menu();
      draw_dive_average();
      break;
    case States::dive_all_cells:
      // draw the dive all cells screen and handle the menu
      handle_menu();
      draw_dive_all_cells();
      break;
    case States::shutdown:
      // draw "Shutdown" and die
      draw_shutdown();
      break;
  }
}

void FSM::key_press()
{
  m_key.press();
  // schedule this to run once immediately, and once after the key delay to make
  // sure that we handle both key presses that can be evaluated immediately and
  // also keypresses that can be evaluated after the press period
  m_context.hw().schedule_once(this);
  m_context.hw().schedule_delay(this, KEY_PRESS_PERIOD_MS);
}

FSM::States FSM::state() const {
  return m_state;
}

LEDHandler &FSM::led() {
  return m_led;
}

void FSM::handle_menu() {
  Key::KeyPress detected = m_key.detected();
  unsigned long ms = m_context.hw().get_millis();
  // are we currently showing the menu?
  if (m_menu_visible) {
    // have the menu expired (no input for 5 seconds)?
    if (m_last_key_input_ms + 5000 < ms) {
      m_menu_visible = false;
    }
    // otherwise, did we get a key press at all?
    else if (detected == Key::KeyPress::no_press) {
      // just draw the current option
      draw_menu();
    }
    // did we get a single press?
    else if (detected == Key::KeyPress::single_press) {
      m_last_key_input_ms = ms;
      // show the next option in the list
      switch (m_option) {
        case MenuOptions::exit:
          m_option = MenuOptions::shutdown;
          break;
        case MenuOptions::shutdown:
          m_option = MenuOptions::calibrate;
          break;
        case MenuOptions::calibrate:
          m_option = MenuOptions::flip_screen;
          break;
        case MenuOptions::flip_screen:
          m_option = MenuOptions::exit;
          break;
      }
      // draw the next option
      draw_menu();
    }
    // did we get a double press?
    else if (detected == Key::KeyPress::double_press) {
      // option has been selected, remove the menu and act on the given option
      m_last_key_input_ms = ms;
      m_menu_visible = false;
      switch (m_option) {
        case MenuOptions::exit:
          // do nothing here, this is just to exit the menu
          break;
        case MenuOptions::shutdown:
          // transition to the shutdown state
          transition(States::shutdown);
          break;
        case MenuOptions::calibrate:
          // transition to the calibration state
          transition(States::calibration);
          break;
        case MenuOptions::flip_screen: {
            // flip the screen
            uint8_t display_orientation = m_context.hw().display_orientation();
            display_orientation = display_orientation == 0 ? 2 : 0;
            m_context.hw().set_display_orientation(display_orientation);
          }
          break;
      }
    }
  }
  // we are not currently showing the menu, but would like to
  else if (detected == Key::KeyPress::double_press) {
    m_last_key_input_ms = ms;
    // reset the menu option and draw the menu
    m_menu_visible = true;
    m_option = MenuOptions::exit;
    draw_menu();
  }
}

void FSM::draw_menu() {
  // draw the current menu option in the top right corner
  HW &hw = m_context.hw();

  hw.set_text_size(0);
  hw.set_cursor(120, 0);
  switch (m_option) {
    case MenuOptions::exit:
      hw.print_text(F("E"));
      break;
    case MenuOptions::shutdown:
      hw.print_text(F("S"));
      break;
    case MenuOptions::calibrate:
      hw.print_text(F("C"));
      break;
    case MenuOptions::flip_screen:
      hw.print_text(F("F"));
      break;
  }

  // note that the flush to display happens when drawing the relevant state later
}

void FSM::draw_calibration() {
  HW &hw = m_context.hw();

  // grab o2 ppO2 readings
  double cell1_ppO2 = m_context.ppo2().o2cell_ppo2(PPO2Monitor::O2Cells::one);
  double cell2_ppO2 = m_context.ppo2().o2cell_ppo2(PPO2Monitor::O2Cells::two);
  double cell3_ppO2 = m_context.ppo2().o2cell_ppo2(PPO2Monitor::O2Cells::three);

  // grab o2 mv values
  double cell1_mv = m_context.ppo2().o2cell_millivolt(PPO2Monitor::O2Cells::one);
  double cell2_mv = m_context.ppo2().o2cell_millivolt(PPO2Monitor::O2Cells::two);
  double cell3_mv = m_context.ppo2().o2cell_millivolt(PPO2Monitor::O2Cells::three);

  // grab battery voltage reading
  double vbat = m_context.vbat();

  // draw calibration screen
  hw.set_text_size(1);

  // draw column headers
  hw.set_cursor(4, 0);
  hw.print_text(F("ppO2"));
  hw.set_cursor(44, 0);
  hw.print_text(F("mV"));
  hw.set_cursor(96, 0);
  hw.print_text(F("Calib"));

  // draw ppO2 values
  hw.set_cursor(0, 8);
  hw.print_double(cell1_ppO2, 5, 2);
  hw.set_cursor(0, 16);
  hw.print_double(cell2_ppO2, 5, 2);
  hw.set_cursor(0, 24);
  hw.print_double(cell3_ppO2, 5, 2);

  // draw mV values
  hw.set_cursor(32, 8);
  hw.print_double(cell1_mv, 6, 2);
  hw.set_cursor(32, 16);
  hw.print_double(cell2_mv, 6, 2);
  hw.set_cursor(32, 24);
  hw.print_double(cell3_mv, 6, 2);

  // draw battery voltage
  hw.set_cursor(90, 16);
  hw.print_double(vbat, 5, 2);
  hw.print_text(F("V"));

  // draw "OK?"
  hw.set_cursor(96, 24);
  hw.print_text(F("OK?"));

  // flush to display
  hw.flush_display();

  // schedule next run in 2000ms
  hw.schedule_delay(this, 2000);
}

void FSM::draw_dive_average() {
  HW &hw = m_context.hw();
  // if this screen has been up for at least 20 seconds, transition to the all
  // cells screen.
  if (m_last_transition_ms + 20000 <= hw.get_millis()) {
    transition(FSM::States::dive_all_cells);
    return;
  }

  // otherwise keep drawing

  // retrieve average ppO2 value
  double avg = m_context.ppo2().o2cell_average();

  // did we get any cell failures?
  if (!m_context.ppo2().cell_failure()) {
    // no failures, everything is peachy. Just show the ppO2 value as large as
    // possible.
    hw.set_cursor(0, 0);
    hw.set_text_size(4);
    hw.print_double(avg, 4, 2);
  } else {
    // there's been a failure of one of the cells - we need to show which, but
    // at the same time show the calculated ppO2 value from the remaining two.
    hw.set_cursor(0, 8);
    hw.set_text_size(2);
    hw.print_double(avg, 4, 2);

    hw.set_cursor(0, 24);
    hw.set_text_size(1);
    switch (m_context.ppo2().which_cell_failed()) {
      case PPO2Monitor::O2Cells::one:
        hw.print_text(F("Fail cell #1"));
        break;
      case PPO2Monitor::O2Cells::two:
        hw.print_text(F("Fail cell #2"));
        break;
      case PPO2Monitor::O2Cells::three:
        hw.print_text(F("Fail cell #3"));
        break;
    }
  }

  hw.flush_display();

  // schedule the next run in 2000ms
  hw.schedule_delay(this, 2000);
}

void FSM::draw_dive_all_cells() {
  HW &hw = m_context.hw();
  // if this screen has been up for at least 10 seconds, transition to the
  // average screen.
  if (m_last_transition_ms + 10000 <= hw.get_millis()) {
    transition(FSM::States::dive_average);
    return;
  }

  // grab o2 ppO2 readings
  double cell1_ppO2 = m_context.ppo2().o2cell_ppo2(PPO2Monitor::O2Cells::one);
  double cell2_ppO2 = m_context.ppo2().o2cell_ppo2(PPO2Monitor::O2Cells::two);
  double cell3_ppO2 = m_context.ppo2().o2cell_ppo2(PPO2Monitor::O2Cells::three);

  // grab battery voltage
  double vbat = m_context.vbat();

  // draw dive all cells screen
  hw.set_text_size(2);

  // draw ppO2 values
  hw.set_cursor(0, 0);
  hw.print_double(cell1_ppO2, 5, 2);
  hw.set_cursor(52, 8);
  hw.print_double(cell2_ppO2, 5, 2);
  hw.set_cursor(0, 16);
  hw.print_double(cell3_ppO2, 5, 2);

  // draw battery voltage
  hw.set_text_size(1);
  hw.set_cursor(90, 24);
  hw.print_double(vbat, 5, 2);
  hw.print_text(F("V"));

  hw.flush_display();

  // schedule the next run in 2000ms.
  hw.schedule_delay(this, 2000);
}

void FSM::draw_shutdown() {
  HW &hw = m_context.hw();

  hw.set_text_size(2);
  hw.set_cursor(16, 8);
  hw.print_text(F("Shutdown"));
  hw.flush_display();

  // wait so the message is obvious
  hw.delay_ms(1500);

  // hang myself
  hw.digital_write(Pins::offswitch, PinLevel::high);

  // no need to schedule any further runs, we're just waiting to die.
}

void FSM::transition(FSM::States next) {
  m_last_transition_ms = m_context.hw().get_millis();
  m_state = next;

  // schedule ourselves to run to evaluate the new state
  m_context.hw().schedule_once(this);
}
