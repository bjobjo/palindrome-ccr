// vim: ts=2 sw=2 expandtab:

#include "HW.h"

#include <Adafruit_SSD1306.h>
#include <Arduino.h>
#include <DeepSleepScheduler.h>
#include <EEPROM.h>

#define AREF_VOLTAGE_MV 4500.0

#define DISPLAY_ADDR 0x3C
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 32

static Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT);

struct o2_calibration {
  double celln1;
  double celln2;
  double celln3;
};

struct eeprom_data {
  uint8_t display_rotation;
  o2_calibration cal;
};

HW::HW() {
  // start serial port
  Serial.begin(9600);
  analogReference(EXTERNAL);
  // initialise display
  display.begin(SSD1306_SWITCHCAPVCC, DISPLAY_ADDR);
  display.setTextColor(SSD1306_WHITE);
  display.display();
}

double HW::adc_read_millivolt(Pins p) {
  return (AREF_VOLTAGE_MV / 1024.0) * analogRead(static_cast<int>(p));
}

void HW::set_pin_mode(Pins p, PinMode mode) {
  pinMode(static_cast<int>(p), static_cast<int>(mode));
}

void HW::digital_write(Pins p, PinLevel level) {
  digitalWrite(static_cast<int>(p), static_cast<int>(level));
}

PinLevel HW::digital_read(Pins p) {
  return static_cast<PinLevel>(digitalRead(static_cast<int>(p)));
}

void HW::attach_interrupt(Pins p, isr_handler h, PinTrigger trigger) {
  attachInterrupt(digitalPinToInterrupt(static_cast<int>(p)), h,
                  static_cast<int>(trigger));
}

void HW::read_o2_calibration(double *celln1, double *celln2, double *celln3) {
  if (!celln1 || !celln2 || !celln3) {
    return;
  }

  // read calibration information from EEPROM and store in given variables
  eeprom_data eeprom;
  EEPROM.get(0, eeprom);
  *celln1 = eeprom.cal.celln1;
  *celln2 = eeprom.cal.celln2;
  *celln3 = eeprom.cal.celln3;
}

void HW::store_o2_calibration(double celln1, double celln2, double celln3) {
  // store the given o2 calibration information in EEPROM
  eeprom_data eeprom;
  EEPROM.get(0, eeprom);
  eeprom.cal.celln1 = celln1;
  eeprom.cal.celln2 = celln2;
  eeprom.cal.celln3 = celln3;
  EEPROM.put(0, eeprom);
}

uint8_t HW::display_orientation()
{
  eeprom_data eeprom;
  EEPROM.get(0, eeprom);
  return eeprom.display_rotation;
}

void HW::set_display_orientation(uint8_t rot)
{
  eeprom_data eeprom;
  EEPROM.get(0, eeprom);
  eeprom.display_rotation = rot;
  display.setRotation(rot);
  EEPROM.put(0, eeprom);
}

void HW::clear_display() {
  display.clearDisplay();
}

void HW::flush_display() {
  display.display();
}

void HW::set_text_size(unsigned int size) {
  display.setTextSize(size);
}

void HW::set_cursor(int x, int y) {
  display.setCursor(x, y);
}

void HW::print_text(const char *text) {
  display.print(text);
}

void HW::print_text(const __FlashStringHelper *text) {
  display.print(text);
}

void HW::print_int(unsigned int i) {
  display.print(i);
}

void HW::print_double(double n, unsigned char width, unsigned int precision) {
  char buf[16];
  width = width > 15 ? 15 : width;
  dtostrf(n, width, precision, buf);
  display.print(buf);
}

void HW::initialise_task_timeout() {
  scheduler.setTaskTimeout(TIMEOUT_2S);
}

void HW::schedule_once(Runnable *r) {
  scheduler.scheduleOnce(r);
}

void HW::schedule_delay(Runnable *r, unsigned long millis) {
  scheduler.scheduleDelayed(r, millis);
}

unsigned long HW::get_millis() const {
  return scheduler.getMillis();
}

void HW::schedule_execute() {
  scheduler.execute();
}

void HW::delay_ms(unsigned long millis) {
  delay(millis);
}
