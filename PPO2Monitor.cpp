#include "PPO2Monitor.h"

PPO2Monitor::PPO2Monitor(HW &hw, double voting_margin)
  : m_hw(hw), m_voting_margin(voting_margin)
{
  // grab calibration information from EEPROM
  m_hw.read_o2_calibration(&m_cell1_calibration, &m_cell2_calibration, &m_cell3_calibration);

  // immediately schedule a run to calculate ppO2 and store millivolt readings
  m_hw.schedule_once(this);
}

double PPO2Monitor::voting_margin() const
{
  return m_voting_margin;
}

bool PPO2Monitor::cell_failure() const
{
  return m_cell_failure;
}

PPO2Monitor::O2Cells PPO2Monitor::which_cell_failed() const
{
  return m_failed_cell;
}

double PPO2Monitor::o2cell_average() const
{
  return m_average_ppo2;
}

double PPO2Monitor::o2cell_millivolt(PPO2Monitor::O2Cells cell) const
{
  switch (cell) {
    case PPO2Monitor::O2Cells::one:
      return m_cell1_millivolt;
      break;
    case PPO2Monitor::O2Cells::two:
      return m_cell2_millivolt;
      break;
    case PPO2Monitor::O2Cells::three:
      return m_cell3_millivolt;
      break;
  }

  return 0.0; // meh
}

double PPO2Monitor::o2cell_ppo2(PPO2Monitor::O2Cells cell) const
{
  switch (cell) {
    case PPO2Monitor::O2Cells::one:
      return m_cell1_ppo2;
      break;
    case PPO2Monitor::O2Cells::two:
      return m_cell2_ppo2;
      break;
    case PPO2Monitor::O2Cells::three:
      return m_cell3_ppo2;
      break;
  }

  return 0.0; // meh
}

void PPO2Monitor::store_o2_calibration()
{
  m_cell1_calibration = m_cell1_raw;
  m_cell2_calibration = m_cell2_raw;
  m_cell3_calibration = m_cell3_raw;

  m_hw.store_o2_calibration(m_cell1_calibration, m_cell2_calibration, m_cell3_calibration);
}

void PPO2Monitor::run()
{
  // read all raw values from the O2 sensors
  m_cell1_raw = m_hw.adc_read_millivolt(static_cast<Pins>(O2Cells::one));
  m_cell2_raw = m_hw.adc_read_millivolt(static_cast<Pins>(O2Cells::two));
  m_cell3_raw = m_hw.adc_read_millivolt(static_cast<Pins>(O2Cells::three));

  // calculate millivolt values from the raw values
  m_cell1_millivolt = m_cell1_raw / (1.0 + (470.0 / 12.0));
  m_cell2_millivolt = m_cell2_raw / (1.0 + (470.0 / 12.0));
  m_cell3_millivolt = m_cell3_raw / (1.0 + (470.0 / 12.0));

  // calculate PPO2 values from the raw values (make sure to handle the calibrated values being zero)
  m_cell1_ppo2 = m_cell1_raw / (m_cell1_calibration + 0.00001);
  m_cell2_ppo2 = m_cell2_raw / (m_cell2_calibration + 0.00001);
  m_cell3_ppo2 = m_cell3_raw / (m_cell3_calibration + 0.00001);

  // perform voting to find the average values and any failed sensors

  // calculate limits for each cell
  double upper1 = m_cell1_ppo2 * (1 + m_voting_margin);
  double lower1 = m_cell1_ppo2 * (1 - m_voting_margin);

  double upper2 = m_cell2_ppo2 * (1 + m_voting_margin);
  double lower2 = m_cell2_ppo2 * (1 - m_voting_margin);

  double upper3 = m_cell3_ppo2 * (1 + m_voting_margin);
  double lower3 = m_cell3_ppo2 * (1 - m_voting_margin);

  // cell 1
  if ((m_cell1_ppo2 > upper2 && m_cell1_ppo2 > upper3) ||
      (m_cell1_ppo2 < lower2 && m_cell1_ppo2 < lower3)) {
    if (m_cell1_fail_count < 10) {
      ++m_cell1_fail_count;
    }
    m_average_ppo2 = (m_cell2_ppo2 + m_cell3_ppo2) / 2.0;
  }
  // cell 2
  else if ((m_cell2_ppo2 > upper1 && m_cell2_ppo2 > upper3) ||
           (m_cell2_ppo2 < lower1 && m_cell2_ppo2 < lower3)) {
    if (m_cell2_fail_count < 10) {
      ++m_cell2_fail_count;
    }
    m_average_ppo2 = (m_cell1_ppo2 + m_cell3_ppo2) / 2.0;
  }
  // cell 3
  else if ((m_cell3_ppo2 > upper1 && m_cell3_ppo2 > upper2) ||
           (m_cell3_ppo2 < lower1 && m_cell3_ppo2 < lower2)) {
    if (m_cell3_fail_count < 10) {
      ++m_cell3_fail_count;
    }
    m_average_ppo2 = (m_cell1_ppo2 + m_cell2_ppo2) / 2.0;
  }
  // all cells OK
  else {
    m_cell1_fail_count = 0;
    m_cell2_fail_count = 0;
    m_cell3_fail_count = 0;

    m_average_ppo2 = (m_cell1_ppo2 + m_cell2_ppo2 + m_cell3_ppo2) / 3.0;
  }

  // if any cells have reached the maximum failure count, mark them as failed
  if (m_cell1_fail_count == 10) {
    m_cell_failure = true;
    m_failed_cell = O2Cells::one;
  }
  else if (m_cell2_fail_count == 10) {
    m_cell_failure = true;
    m_failed_cell = O2Cells::two;
  }
  else if (m_cell3_fail_count == 10) {
    m_cell_failure = true;
    m_failed_cell = O2Cells::three;
  }
  else {
    m_cell_failure = false;
  }

  // schedule next run of this in 1000ms
  m_hw.schedule_delay(this, 1000);
}
