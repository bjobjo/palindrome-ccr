#ifndef KEY_H
#define KEY_H

#include "HW.h"

#define KEY_PRESS_PERIOD_MS 500

class Key {
  public:
    Key(HW &hw);

    enum class KeyPress {
      no_press,             //< no keypresses detected.
      intermediate_press,   //< we're in the process of detecting keypresses
      single_press,         //< single keypress detected (1 press during 500ms)
      double_press,         //< double keypress detected (2 press during 500ms)
    };

    // these may be called in interrupt context, so we need to be careful to avoid
    // calls which are not safe during interrupts
    void press();

    // checks if we've hit the timeout for the calibration press
    void tick();

    // returns the detected key press and resets the internal state if necessary
    KeyPress detected();

    unsigned long count() const;

  private:
    HW &m_hw;

    unsigned long m_first_press_millis{0};
    unsigned long m_count_press{0};
    KeyPress m_press{KeyPress::no_press};
};

#endif // KEY_H
