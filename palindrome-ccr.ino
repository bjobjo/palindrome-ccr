// vim: ts=2 sw=2 expandtab:

#include "FSM.h"
#include "HW.h"

HW *hw{nullptr};
PPO2Monitor *ppo2{nullptr};
Key *key{nullptr};
FSM *fsm{nullptr};

#define DEBOUNCE_INTERVAL_MS 50

void isr_sw_press() {
  static unsigned long last_press = 0;
  unsigned long current_press = hw->get_millis();
  // debounce/ignore key presses that are too close together
  if ((last_press + DEBOUNCE_INTERVAL_MS) < current_press) {
    fsm->key_press();
  }
  last_press = current_press;
}

void setup() {
  // initialise components
  hw = new HW;
  ppo2 = new PPO2Monitor(*hw, 0.2);
  key = new Key(*hw);

  // set up pins
  hw->set_pin_mode(Pins::ledGreen, PinMode::output);
  hw->set_pin_mode(Pins::ledRed, PinMode::output);
  hw->set_pin_mode(Pins::offswitch, PinMode::output);
  hw->set_pin_mode(Pins::piezopull, PinMode::output);

  hw->set_pin_mode(Pins::piezo, PinMode::input);
  hw->set_pin_mode(Pins::celln3, PinMode::input);
  hw->set_pin_mode(Pins::celln2, PinMode::input);
  hw->set_pin_mode(Pins::celln1, PinMode::input);
  hw->set_pin_mode(Pins::battn, PinMode::input);

  hw->digital_write(Pins::offswitch, PinLevel::low);
  hw->digital_write(Pins::piezopull, PinLevel::low);

  // restore screen orientation
  hw->set_display_orientation(hw->display_orientation());
  
  // read calibration info
  double cal_celln1, cal_celln2, cal_celln3;
  hw->read_o2_calibration(&cal_celln1, &cal_celln2, &cal_celln3);
  // do we have valid information?
  if ((cal_celln1 > 2500.0 || cal_celln1 < 1500.0) ||
      (cal_celln2 > 2500.0 || cal_celln2 < 1500.0) ||
      (cal_celln3 > 2500.0 || cal_celln3 < 1500.0)) {
    // nope, go directly to calibration state
    fsm = new FSM(*hw, *ppo2, *key, FSM::States::calibration);
  } else {
    // looking good, start with the default state
    fsm = new FSM(*hw, *ppo2, *key);
  }

  // attach interrupts
  hw->attach_interrupt(Pins::piezo, isr_sw_press, PinTrigger::rising);

  // initialise task watchdog
  hw->initialise_task_timeout();
}

void loop() {
  // start the scheduler
  hw->schedule_execute();
}
