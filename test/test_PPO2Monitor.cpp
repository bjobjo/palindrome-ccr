#include <gtest/gtest.h>

#include <HW.h>

#include "../PPO2Monitor.cpp"

using namespace testing;

class PPO2MonitorTest : public Test {
public:
  PPO2MonitorTest() {
    EXPECT_CALL(m_hw, read_o2_calibration(_, _, _))
        .WillOnce(DoAll(SetArgPointee<0>(1125.0), SetArgPointee<1>(1150.0),
                        SetArgPointee<2>(1175.0)));
    EXPECT_CALL(m_hw, schedule_once(_));

    m_ppo2 = new ::PPO2Monitor(m_hw);
  }
  virtual ~PPO2MonitorTest() = default;

protected:
  // helper function to expect a read of the ppo2 sensors
  void expect_sensor_read(double celln1, double celln2, double celln3) {
    EXPECT_CALL(m_hw, adc_read_millivolt(Pins::celln1))
        .WillOnce(Return(celln1));
    EXPECT_CALL(m_hw, adc_read_millivolt(Pins::celln2))
        .WillOnce(Return(celln2));
    EXPECT_CALL(m_hw, adc_read_millivolt(Pins::celln3))
        .WillOnce(Return(celln3));

    EXPECT_CALL(m_hw, schedule_delay(_, 1000));
  }

  StrictMock<HW> m_hw;
  ::PPO2Monitor *m_ppo2{nullptr};
};

TEST_F(PPO2MonitorTest, voting_margin) {
  // default voting margin is 0.2
  EXPECT_FLOAT_EQ(m_ppo2->voting_margin(), 0.2);
}

TEST_F(PPO2MonitorTest, cell_failure) {
  // by default, no cells have failed
  EXPECT_FALSE(m_ppo2->cell_failure());

  // a cell shall be considered failed if it is outside of the limits set by the
  // margin relative to the other two cells for 10 consecutive runs
  for (int i = 0; i < 9; i++) {
    expect_sensor_read(290.0, 100.0, 310.0);
    m_ppo2->run();
    EXPECT_FALSE(m_ppo2->cell_failure());
  }

  // on the 10th run, the cell shall be marked as failed
  expect_sensor_read(290.0, 100.0, 310.0);
  m_ppo2->run();
  EXPECT_TRUE(m_ppo2->cell_failure());
}

TEST_F(PPO2MonitorTest, which_cell_failed) {
  // if a cell has failed, which cell it was shall be returned

  // a cell shall be considered failed if it is outside of the limits set by the
  // margin relative to the other two cells for 10 consecutive runs
  for (int i = 0; i < 10; i++) {
    expect_sensor_read(290.0, 100.0, 310.0);
    m_ppo2->run();
  }

  ASSERT_TRUE(m_ppo2->cell_failure());
  EXPECT_EQ(m_ppo2->which_cell_failed(), PPO2Monitor::O2Cells::two);
}

TEST_F(PPO2MonitorTest, o2cell_average) {
  // the o2cell average shall return the average ppo2 over the cells that have
  // not failed
  expect_sensor_read(290.0, 300.0, 310.0);
  m_ppo2->run();
  EXPECT_NEAR(m_ppo2->o2cell_average(), 0.26, 0.009);

  // if a cell does fail, it shall not be included in the calculation, so the
  // result shall still be reliable
  for (int i = 0; i < 10; i++) {
    expect_sensor_read(290.0, 100.0, 310.0);
    m_ppo2->run();
  }

  EXPECT_NEAR(m_ppo2->o2cell_average(), 0.26, 0.009);
}

TEST_F(PPO2MonitorTest, o2cell_millivolt) {
  // the o2cell millivolt reading shall calculate the actual millivolt value of
  // the cell (compensating for the circuitry increasing the domain)
  expect_sensor_read(290.0, 300.0, 310.0);
  m_ppo2->run();

  EXPECT_NEAR(m_ppo2->o2cell_millivolt(PPO2Monitor::O2Cells::one), 7.22, 0.009);
  EXPECT_NEAR(m_ppo2->o2cell_millivolt(PPO2Monitor::O2Cells::two), 7.46, 0.009);
  EXPECT_NEAR(m_ppo2->o2cell_millivolt(PPO2Monitor::O2Cells::three), 7.71,
              0.009);
}

TEST_F(PPO2MonitorTest, o2cell_ppo2) {
  // the o2cell ppo2 reading shall calculate the ppo2 value for each cell
  // regardless of voting
  expect_sensor_read(290.0, 300.0, 310.0);
  m_ppo2->run();

  EXPECT_NEAR(m_ppo2->o2cell_ppo2(PPO2Monitor::O2Cells::one), 0.25, 0.009);
  EXPECT_NEAR(m_ppo2->o2cell_ppo2(PPO2Monitor::O2Cells::two), 0.26, 0.009);
  EXPECT_NEAR(m_ppo2->o2cell_ppo2(PPO2Monitor::O2Cells::three), 0.26, 0.009);
}

TEST_F(PPO2MonitorTest, store_o2_calibration)
{
  // storing the o2 calibration shall use the latest raw values read from the sensors and
  // store them in the eeprom via m_hw.
  expect_sensor_read(290.0, 300.0, 310.0);
  m_ppo2->run();

  EXPECT_CALL(m_hw, store_o2_calibration(290.0, 300.0, 310.0));
  m_ppo2->store_o2_calibration();
}
