#ifndef ARDUINO_H
#define ARDUINO_H

#define F(a) a

#define A0 8
#define A1 9
#define A2 10
#define A3 11

#define INPUT 0
#define OUTPUT 2

#define HIGH 1
#define LOW 0

#define CHANGE 1
#define RISING 2
#define FALLING 3

#endif // ARDUINO_H
