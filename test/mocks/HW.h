#ifndef HW_H
#define HW_H

#include <gmock/gmock.h>

#include <DeepSleepScheduler.h>
#include <Pins.h>

typedef void (*isr_handler)();

class HW {
public:
  MOCK_METHOD(double, adc_read_millivolt, (Pins p));

  MOCK_METHOD(void, set_pin_mode, (Pins p, PinMode mode));
  MOCK_METHOD(void, digital_write, (Pins p, PinLevel level));
  MOCK_METHOD(PinLevel, digital_read, (Pins p));
  MOCK_METHOD(void, attach_interrupt, (Pins p, isr_handler h, PinTrigger trigger));

  MOCK_METHOD(void, read_o2_calibration,
              (double *celln1, double *celln2, double *celln3));
  MOCK_METHOD(void, store_o2_calibration,
              (double celln1, double celln2, double celln3));
  MOCK_METHOD(uint8_t, display_orientation, ());
  MOCK_METHOD(void, set_display_orientation, (uint8_t));

  MOCK_METHOD(void, clear_display, ());
  MOCK_METHOD(void, flush_display, ());
  MOCK_METHOD(void, set_text_size, (unsigned int size));
  MOCK_METHOD(void, set_cursor, (int x, int y));
  MOCK_METHOD(void, print_text, (const char *text));
  MOCK_METHOD(void, print_double,
              (double n, unsigned char width, unsigned int precision));

  MOCK_METHOD(void, initialise_task_timeout, ());
  MOCK_METHOD(void, schedule_once, (Runnable * r));
  MOCK_METHOD(void, schedule_delay, (Runnable * r, unsigned long millis));
  MOCK_METHOD(unsigned long, get_millis, (), (const));
  MOCK_METHOD(void, schedule_execute, ());
  MOCK_METHOD(void, delay_ms, (unsigned long));
};

#endif // HW_H
