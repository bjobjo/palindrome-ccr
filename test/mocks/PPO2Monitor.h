#ifndef PPO2MONITOR_H
#define PPO2MONITOR_H

#include <gmock/gmock.h>

#include <HW.h>
#include <Pins.h>

class PPO2Monitor : public Runnable {
public:
    // the three O2 cells we have available
    enum class O2Cells {
      one = static_cast<int>(Pins::celln1),
      two = static_cast<int>(Pins::celln2),
      three = static_cast<int>(Pins::celln3),
    };

    PPO2Monitor(HW &, double margin = 0.2) {}
    virtual ~PPO2Monitor() = default;

    MOCK_METHOD(double, voting_margin, (), (const));
    MOCK_METHOD(bool, cell_failure, (), (const));
    MOCK_METHOD(O2Cells, which_cell_failed, (), (const));

    MOCK_METHOD(double, o2cell_average, (), (const));
    MOCK_METHOD(double, o2cell_millivolt, (O2Cells), (const));
    MOCK_METHOD(double, o2cell_ppo2, (O2Cells), (const));

    MOCK_METHOD(void, store_o2_calibration, ());

    MOCK_METHOD(void, run, (), (override));
};

#endif // PPO2MONITOR_H
