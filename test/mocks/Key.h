#ifndef KEY_H
#define KEY_H

#include <HW.h>

#define KEY_PRESS_PERIOD_MS 500

class Key {
public:
  enum class KeyPress {
    no_press,             //< no keypresses detected.
    intermediate_press,   //< we're in the process of detecting keypresses
    single_press,         //< single keypress detected (1 press during 500ms)
    double_press,         //< double keypress detected (2 press during 500ms)
  };

  Key(HW &) {}

  MOCK_METHOD(void, press, ());
  MOCK_METHOD(void, tick, ());
  MOCK_METHOD(KeyPress, detected, ());
};

#endif // KEY_H
