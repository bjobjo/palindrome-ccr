#ifndef FSM_H
#define FSM_H

#include <gmock/gmock.h>

#include <DeepSleepScheduler.h>
#include <HW.h>
#include <Key.h>
#include <PPO2Monitor.h>

class FSM : public Runnable {
public:
  enum class States {
    calibration,
    dive_average,
    dive_all_cells,
    shutdown,
  };

  FSM(HW &hw, PPO2Monitor &ppo2, Key &key, States initial = States::dive_average)
      : m_initial(initial) {}
  virtual ~FSM() = default;

  MOCK_METHOD(void, run, (), (override));
  MOCK_METHOD(void, key_press, ());
  MOCK_METHOD(void, key_release, ());

  const States initial_state() const { return m_initial; }

private:
  const States m_initial;
};

#endif // FSM_H
