#ifndef DEEP_SLEEP_SCHEDULER_H
#define DEEP_SLEEP_SCHEDULER_H

class Runnable {
  virtual void run() = 0;
};

#endif // DEEP_SLEEP_SCHEDULER_H
