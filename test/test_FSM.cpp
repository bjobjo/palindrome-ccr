// vim: ts=2 sw=2 expandtab:

#include <gtest/gtest.h>

#include <HW.h>
#include <Key.h>
#include <PPO2Monitor.h>

#include "../FSM.cpp"

using namespace testing;

class FsmTest : public Test {
public:
  FsmTest() {
    // the state machine will schedule itself and the LED handler immediately
    // upon creation
    EXPECT_CALL(m_hw, schedule_once(_)).Times(2);
  }

  ~FsmTest() { delete m_fsm; }

protected:
  void
  expect_common_checks(Key::KeyPress detected_key = Key::KeyPress::no_press) {
    // once we run the state machine to handle inputs and draw to the screen,
    // the state machine shall:
    // - clear the display
    // - tick the key press detector to check for key presses
    // - check for any key presses
    EXPECT_CALL(m_hw, clear_display());
    EXPECT_CALL(m_key, tick());
    EXPECT_CALL(m_key, detected()).WillOnce(Return(detected_key));
  }

  StrictMock<HW> m_hw;
  StrictMock<::Key> m_key{m_hw};
  StrictMock<PPO2Monitor> m_ppo2{m_hw};
  FSM *m_fsm{nullptr};
};

TEST_F(FsmTest, led_initial_cycle) {
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::dive_average);
  LEDHandler &led = m_fsm->led();

  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::read_ppO2);
}

TEST_F(FsmTest, led_ppO2_above_16) {
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::dive_average);
  LEDHandler &led = m_fsm->led();

  EXPECT_CALL(m_ppo2, o2cell_average()).WillOnce(Return(1.78));
  EXPECT_CALL(m_ppo2, cell_failure()).WillOnce(Return(false));
  EXPECT_CALL(m_hw, schedule_once(&led));

  led.run();

  // we should now be in the blink_red state, due to blink on and off 14 times
  // (ppO2 of 1.78 makes for 7 blinks on, 7 blinks off.)
  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::blink_red);

  // if we run the handler again, it should start blinking the LED
  PinLevel level = PinLevel::low;
  for (int i = 0; i < 14; i++) {
    EXPECT_CALL(m_hw, digital_write(Pins::ledRed, level));
    EXPECT_CALL(m_hw, schedule_delay(&led, 300));
    if (level == PinLevel::high) {
      level = PinLevel::low;
    } else {
      level = PinLevel::high;
    }

    led.run();
  }

  // the final run of the led handler will change the state and schedule the
  // code to run immediately again.
  EXPECT_CALL(m_hw, schedule_once(&led));
  led.run();

  // we should now be in the delay prior to the next read
  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::delay_until_next_read);

  // and if we run again, we should explicitly disable both LEDs for safety and
  // go to the read_ppO2 state after 5 seconds.
  EXPECT_CALL(m_hw, digital_write(Pins::ledRed, PinLevel::high));
  EXPECT_CALL(m_hw, digital_write(Pins::ledGreen, PinLevel::high));
  EXPECT_CALL(m_hw, schedule_delay(&led, 2000));

  led.run();

  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::read_ppO2);
}

TEST_F(FsmTest, led_ppO2_between_10_and_16) {
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::dive_average);
  LEDHandler &led = m_fsm->led();

  EXPECT_CALL(m_ppo2, o2cell_average()).WillOnce(Return(1.52));
  EXPECT_CALL(m_ppo2, cell_failure()).WillOnce(Return(false));
  EXPECT_CALL(m_hw, schedule_once(&led));

  led.run();

  // we should now be in the blink_red state, due to blink on and off 10 times
  // (ppO2 of 1.5 makes for 5 blinks on, 5 blinks off.)
  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::blink_green);

  // if we run the handler again, it should start blinking the LED
  PinLevel level = PinLevel::low;
  for (int i = 0; i < 10; i++) {
    EXPECT_CALL(m_hw, digital_write(Pins::ledGreen, level));
    EXPECT_CALL(m_hw, schedule_delay(&led, 300));
    if (level == PinLevel::high) {
      level = PinLevel::low;
    } else {
      level = PinLevel::high;
    }

    led.run();
  }

  // the final run of the led handler will change the state and schedule the
  // code to run immediately again.
  EXPECT_CALL(m_hw, schedule_once(&led));
  led.run();

  // we should now be in the delay prior to the next read
  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::delay_until_next_read);

  // and if we run again, we should explicitly disable both LEDs for safety and
  // go to the read_ppO2 state after 5 seconds.
  EXPECT_CALL(m_hw, digital_write(Pins::ledRed, PinLevel::high));
  EXPECT_CALL(m_hw, digital_write(Pins::ledGreen, PinLevel::high));
  EXPECT_CALL(m_hw, schedule_delay(&led, 2000));

  led.run();

  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::read_ppO2);
}

TEST_F(FsmTest, led_ppO2_at_10) {
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::dive_average);
  LEDHandler &led = m_fsm->led();

  EXPECT_CALL(m_ppo2, o2cell_average()).WillOnce(Return(1.0));
  EXPECT_CALL(m_ppo2, cell_failure()).WillOnce(Return(false));
  EXPECT_CALL(m_hw, schedule_once(&led));

  led.run();

  // we should now bein the blink alternate state, where we show the green LED
  // for twice as long as the red LED.
  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::blink_alternate);

  // if we run the handler again, it should start with the green LED and a
  // longer delay
  EXPECT_CALL(m_hw, digital_write(Pins::ledGreen, PinLevel::low));
  EXPECT_CALL(m_hw, digital_write(Pins::ledRed, PinLevel::high));
  EXPECT_CALL(m_hw, schedule_delay(&led, 200));
  led.run();

  // the next run should show the red LED and with a shorter delay
  EXPECT_CALL(m_hw, digital_write(Pins::ledGreen, PinLevel::high));
  EXPECT_CALL(m_hw, digital_write(Pins::ledRed, PinLevel::low));
  EXPECT_CALL(m_hw, schedule_delay(&led, 100));
  led.run();

  // the final run of the led handler will change the state and schedule the
  // code to run immediately again.
  EXPECT_CALL(m_hw, schedule_once(&led));
  led.run();

  // we should now be in the delay prior to the next read
  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::delay_until_next_read);

  // and if we run again, we should explicitly disable both LEDs for safety and
  // go to the read_ppO2 state after 5 seconds.
  EXPECT_CALL(m_hw, digital_write(Pins::ledRed, PinLevel::high));
  EXPECT_CALL(m_hw, digital_write(Pins::ledGreen, PinLevel::high));
  EXPECT_CALL(m_hw, schedule_delay(&led, 2000));

  led.run();

  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::read_ppO2);
}

TEST_F(FsmTest, led_ppO2_between04_and_10) {
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::dive_average);
  LEDHandler &led = m_fsm->led();

  EXPECT_CALL(m_ppo2, o2cell_average()).WillOnce(Return(0.65));
  EXPECT_CALL(m_ppo2, cell_failure()).WillOnce(Return(false));
  EXPECT_CALL(m_hw, schedule_once(&led));

  led.run();

  // we should now be in the blink_yellow state, due to blink on and off 8
  // times (ppO2 of 0.6 makes for 4 blinks on, 4 blinks off.)
  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::blink_yellow);

  // if we run the handler again, it should start blinking the LED
  PinLevel level = PinLevel::low;
  for (int i = 0; i < 8; i++) {
    EXPECT_CALL(m_hw, digital_write(Pins::ledRed, level));
    EXPECT_CALL(m_hw, digital_write(Pins::ledGreen, level));
    EXPECT_CALL(m_hw, schedule_delay(&led, 100));
    if (level == PinLevel::high) {
      level = PinLevel::low;
    } else {
      level = PinLevel::high;
    }

    led.run();
  }

  // the final run of the led handler will change the state and schedule the
  // code to run immediately again.
  EXPECT_CALL(m_hw, schedule_once(&led));
  led.run();

  // we should now be in the delay prior to the next read
  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::delay_until_next_read);

  // and if we run again, we should explicitly disable both LEDs for safety and
  // go to the read_ppO2 state after 5 seconds.
  EXPECT_CALL(m_hw, digital_write(Pins::ledRed, PinLevel::high));
  EXPECT_CALL(m_hw, digital_write(Pins::ledGreen, PinLevel::high));
  EXPECT_CALL(m_hw, schedule_delay(&led, 2000));

  led.run();

  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::read_ppO2);
}

TEST_F(FsmTest, led_ppO2_below_04) {
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::dive_average);
  LEDHandler &led = m_fsm->led();

  EXPECT_CALL(m_ppo2, o2cell_average()).WillOnce(Return(0.26));
  EXPECT_CALL(m_ppo2, cell_failure()).WillOnce(Return(false));
  EXPECT_CALL(m_hw, schedule_once(&led));

  led.run();

  // we should now be in the blink_red state, due to blink on and off 16 times
  // (ppO2 of 0.2 makes for 8 blinks on, 8 blinks off.)
  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::blink_red);

  // if we run the handler again, it should start blinking the LED
  PinLevel level = PinLevel::low;
  for (int i = 0; i < 16; i++) {
    EXPECT_CALL(m_hw, digital_write(Pins::ledRed, level));
    EXPECT_CALL(m_hw, schedule_delay(&led, 300));
    if (level == PinLevel::high) {
      level = PinLevel::low;
    } else {
      level = PinLevel::high;
    }

    led.run();
  }

  // the final run of the led handler will change the state and schedule the
  // code to run immediately again.
  EXPECT_CALL(m_hw, schedule_once(&led));
  led.run();

  // we should now be in the delay prior to the next read
  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::delay_until_next_read);

  // and if we run again, we should explicitly disable both LEDs for safety and
  // go to the read_ppO2 state after 5 seconds.
  EXPECT_CALL(m_hw, digital_write(Pins::ledRed, PinLevel::high));
  EXPECT_CALL(m_hw, digital_write(Pins::ledGreen, PinLevel::high));
  EXPECT_CALL(m_hw, schedule_delay(&led, 2000));

  led.run();

  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::read_ppO2);
}

TEST_F(FsmTest, led_cell_failure) {
  // if there is a failure detected for any of the cells, we shall show the cell
  // failure pattern prior to showing the result.
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::dive_average);
  LEDHandler &led = m_fsm->led();

  EXPECT_CALL(m_ppo2, o2cell_average()).WillOnce(Return(1.0));
  EXPECT_CALL(m_ppo2, cell_failure()).WillOnce(Return(true));
  EXPECT_CALL(m_hw, schedule_once(&led));

  led.run();

  // we should now be in the cell failure state
  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::cell_failure);

  // the cell failure mode shall aternate between showing green and red with a
  // 20Hz refresh rate 5 times each
  PinLevel level_1 = PinLevel::low;
  PinLevel level_2 = PinLevel::high;
  for (int i = 0; i < 10; i++) {
    EXPECT_CALL(m_hw, digital_write(Pins::ledRed, level_1));
    EXPECT_CALL(m_hw, digital_write(Pins::ledGreen, level_2));
    EXPECT_CALL(m_hw, schedule_delay(&led, 50));
    PinLevel tmp = level_1;
    level_1 = level_2;
    level_2 = tmp;

    led.run();
  }

  // the final run of the led handler shall disable both leds and wait for 300ms
  // until it goes to the next state.
  EXPECT_CALL(m_hw, digital_write(Pins::ledRed, PinLevel::high));
  EXPECT_CALL(m_hw, digital_write(Pins::ledGreen, PinLevel::high));
  EXPECT_CALL(m_hw, schedule_delay(&led, 300));
  led.run();

  // we should now be in the blink alternate state due to the ppO2 being 1.0.
  EXPECT_EQ(led.blink_cycle(), LEDHandler::State::blink_alternate);
}

TEST_F(FsmTest, calibration) {
  // running the state machine with the calibration screen shall draw the
  // correct information to the screen.
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::calibration);

  expect_common_checks();

  // the calibration screen shall draw:
  // - ppO2 values for each of the three O2 sensors
  // - mV values for each of the three O2 sensors
  // - battery voltage
  // - headers for the ppO2 and mV columns, plus text identifying it as the
  //   calibration screen
  // - text to ask if the result is OK.
  // ...before scheduling the state machine to run again in 200ms.

  // ppO2 reading and millivolt reading
  EXPECT_CALL(m_ppo2, o2cell_ppo2(PPO2Monitor::O2Cells::one))
      .WillOnce(Return(0.25));
  EXPECT_CALL(m_ppo2, o2cell_ppo2(PPO2Monitor::O2Cells::two))
      .WillOnce(Return(0.26));
  EXPECT_CALL(m_ppo2, o2cell_ppo2(PPO2Monitor::O2Cells::three))
      .WillOnce(Return(0.26));

  EXPECT_CALL(m_ppo2, o2cell_millivolt(PPO2Monitor::O2Cells::one))
      .WillOnce(Return(7.21));
  EXPECT_CALL(m_ppo2, o2cell_millivolt(PPO2Monitor::O2Cells::two))
      .WillOnce(Return(7.46));
  EXPECT_CALL(m_ppo2, o2cell_millivolt(PPO2Monitor::O2Cells::three))
      .WillOnce(Return(7.71));

  // battery voltage reading
  EXPECT_CALL(m_hw, adc_read_millivolt(Pins::battn)).WillOnce(Return(5124));

  // drawing headers
  {
    InSequence seq;
    EXPECT_CALL(m_hw, set_text_size(1));
    EXPECT_CALL(m_hw, set_cursor(4, 0));
    EXPECT_CALL(m_hw, print_text(StrEq("ppO2")));
    EXPECT_CALL(m_hw, set_cursor(44, 0));
    EXPECT_CALL(m_hw, print_text(StrEq("mV")));
    EXPECT_CALL(m_hw, set_cursor(96, 0));
    EXPECT_CALL(m_hw, print_text(StrEq("Calib")));
  }

  // drawing ppO2 values
  {
    InSequence seq;
    EXPECT_CALL(m_hw, set_cursor(0, 8));
    EXPECT_CALL(m_hw, print_double(DoubleNear(0.25, 0.001), 5, 2));
    EXPECT_CALL(m_hw, set_cursor(0, 16));
    EXPECT_CALL(m_hw, print_double(DoubleNear(0.26, 0.001), 5, 2));
    EXPECT_CALL(m_hw, set_cursor(0, 24));
    EXPECT_CALL(m_hw, print_double(DoubleNear(0.26, 0.001), 5, 2));
  }

  {
    InSequence seq;
    // draw mV values
    EXPECT_CALL(m_hw, set_cursor(32, 8));
    EXPECT_CALL(m_hw, print_double(DoubleNear(7.21, 0.0001), 6, 2));
    EXPECT_CALL(m_hw, set_cursor(32, 16));
    EXPECT_CALL(m_hw, print_double(DoubleNear(7.46, 0.0001), 6, 2));
    EXPECT_CALL(m_hw, set_cursor(32, 24));
    EXPECT_CALL(m_hw, print_double(DoubleNear(7.71, 0.0001), 6, 2));
  }

  // draw battery voltage
  {
    InSequence seq;
    EXPECT_CALL(m_hw, set_cursor(90, 16));
    EXPECT_CALL(m_hw, print_double(DoubleNear(5.124, 0.0001), 5, 2));
    EXPECT_CALL(m_hw, print_text(StrEq("V")));
  }

  // draw OK
  {
    InSequence seq;
    EXPECT_CALL(m_hw, set_cursor(96, 24));
    EXPECT_CALL(m_hw, print_text(StrEq("OK?")));
  }

  // flush to display
  EXPECT_CALL(m_hw, flush_display());

  // schedule next run
  EXPECT_CALL(m_hw, schedule_delay(m_fsm, 2000));

  m_fsm->run();
}

TEST_F(FsmTest, calibration_accept) {
  // when a double key press is detected during the calibration process, the
  // state machine shall store the calibration result and the state shall
  // change to the dive average state.
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::calibration);

  expect_common_checks(Key::KeyPress::double_press);

  {
    InSequence seq;
    // store the calibration result
    EXPECT_CALL(m_ppo2, store_o2_calibration());
  }

  EXPECT_CALL(m_hw, get_millis()).WillOnce(Return(100));
  EXPECT_CALL(m_hw, schedule_once(m_fsm));

  m_fsm->run();

  EXPECT_EQ(m_fsm->state(), FSM::States::dive_average);
}

TEST_F(FsmTest, dive_average_no_failure) {
  // running the state machine with the dive average screen shall draw the
  // correct information to the screen.
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::dive_average);

  expect_common_checks();

  EXPECT_CALL(m_hw, get_millis()).Times(2).WillRepeatedly(Return(100));
  EXPECT_CALL(m_ppo2, o2cell_average()).WillOnce(Return(0.26));
  EXPECT_CALL(m_ppo2, cell_failure()).WillOnce(Return(false));

  {
    InSequence seq;
    EXPECT_CALL(m_hw, set_cursor(0, 0));
    EXPECT_CALL(m_hw, set_text_size(4));
    EXPECT_CALL(m_hw, print_double(DoubleNear(0.26, 0.001), 4, 2));
    EXPECT_CALL(m_hw, flush_display());
  }

  EXPECT_CALL(m_hw, schedule_delay(m_fsm, 2000));

  m_fsm->run();
}

TEST_F(FsmTest, dive_average_cell_failure) {
  // an O2 cell failure shall be presented to the user.
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::dive_average);

  expect_common_checks();

  EXPECT_CALL(m_hw, get_millis()).Times(2).WillRepeatedly(Return(100));
  EXPECT_CALL(m_ppo2, o2cell_average()).WillOnce(Return(0.26));
  EXPECT_CALL(m_ppo2, cell_failure()).WillOnce(Return(true));
  EXPECT_CALL(m_ppo2, which_cell_failed())
      .WillOnce(Return(PPO2Monitor::O2Cells::two));

  {
    InSequence seq;
    EXPECT_CALL(m_hw, set_cursor(0, 8));
    EXPECT_CALL(m_hw, set_text_size(2));
    EXPECT_CALL(m_hw, print_double(0.26, 4, 2));

    EXPECT_CALL(m_hw, set_cursor(0, 24));
    EXPECT_CALL(m_hw, set_text_size(1));
    EXPECT_CALL(m_hw, print_text(StrEq("Fail cell #2")));

    EXPECT_CALL(m_hw, flush_display());
  }

  EXPECT_CALL(m_hw, schedule_delay(m_fsm, 2000));

  m_fsm->run();
}

TEST_F(FsmTest, dive_average_all_cells_transition) {
  // if it has been shown enough times, the dive average screen shall
  // automatically transition to the all cells screen.
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::dive_average);

  expect_common_checks();

  // when the screen has been up for 20 seconds, we shall transition to the dive
  // all cells screen
  EXPECT_CALL(m_hw, get_millis()).Times(3).WillRepeatedly(Return(20'000));
  EXPECT_CALL(m_hw, schedule_once(m_fsm));
  m_fsm->run();

  EXPECT_EQ(m_fsm->state(), FSM::States::dive_all_cells);
}

TEST_F(FsmTest, dive_all_cells) {
  // running the state machine with the dive all cells screen shall draw the
  // correct information to the screen.
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::dive_all_cells);

  expect_common_checks();

  EXPECT_CALL(m_hw, get_millis()).Times(2).WillRepeatedly(Return(100));

  // the all cells screen shall read the ppO2 values and show all of them plus
  // the battery voltage.
  EXPECT_CALL(m_ppo2, o2cell_ppo2(PPO2Monitor::O2Cells::one))
      .WillOnce(Return(0.25));
  EXPECT_CALL(m_ppo2, o2cell_ppo2(PPO2Monitor::O2Cells::two))
      .WillOnce(Return(0.26));
  EXPECT_CALL(m_ppo2, o2cell_ppo2(PPO2Monitor::O2Cells::three))
      .WillOnce(Return(0.26));

  EXPECT_CALL(m_hw, adc_read_millivolt(Pins::battn)).WillOnce(Return(5125.0));

  {
    InSequence seq;
    EXPECT_CALL(m_hw, set_text_size(2));

    EXPECT_CALL(m_hw, set_cursor(0, 0));
    EXPECT_CALL(m_hw, print_double(DoubleNear(0.25, 0.001), 5, 2));
    EXPECT_CALL(m_hw, set_cursor(52, 8));
    EXPECT_CALL(m_hw, print_double(DoubleNear(0.26, 0.001), 5, 2));
    EXPECT_CALL(m_hw, set_cursor(0, 16));
    EXPECT_CALL(m_hw, print_double(DoubleNear(0.26, 0.001), 5, 2));
  }
  {
    InSequence seq;
    EXPECT_CALL(m_hw, set_text_size(1));
    EXPECT_CALL(m_hw, set_cursor(90, 24));
    EXPECT_CALL(m_hw, print_double(DoubleNear(5.125, 0.0001), 5, 2));
    EXPECT_CALL(m_hw, print_text(StrEq("V")));
  }

  EXPECT_CALL(m_hw, flush_display());

  EXPECT_CALL(m_hw, schedule_delay(m_fsm, 2000));

  m_fsm->run();
}

TEST_F(FsmTest, dive_all_cells_average_transition) {
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::dive_all_cells);

  // if it has been shown enough times, the all cells screen shall automatically
  // transition to the average screen.
  expect_common_checks();

  // when the screen has been up for 10 seconds, we shall transition to the dive
  // all cells screen
  EXPECT_CALL(m_hw, get_millis()).Times(3).WillRepeatedly(Return(10'000));
  EXPECT_CALL(m_hw, schedule_once(m_fsm));
  m_fsm->run();

  EXPECT_EQ(m_fsm->state(), FSM::States::dive_average);
}

TEST_F(FsmTest, shutdown) {
  // running the state machine with the shutdown screen shall draw the correct
  // information to the screen.
  m_fsm = new FSM(m_hw, m_ppo2, m_key, FSM::States::shutdown);

  EXPECT_CALL(m_hw, clear_display());
  EXPECT_CALL(m_key, tick());

  {
    InSequence seq;
    EXPECT_CALL(m_hw, set_text_size(2));
    EXPECT_CALL(m_hw, set_cursor(16, 8));
    EXPECT_CALL(m_hw, print_text(StrEq("Shutdown")));
    EXPECT_CALL(m_hw, flush_display());
    EXPECT_CALL(m_hw, delay_ms(1500));

    EXPECT_CALL(m_hw, digital_write(Pins::offswitch, PinLevel::high));
  }

  m_fsm->run();
}

// TODO: menu tests
