#include <gtest/gtest.h>

#include <HW.h>

#include "../Key.cpp"

using namespace testing;

class KeyTest : public Test {
protected:
  StrictMock<HW> m_hw;
  ::Key m_key{m_hw};
};

TEST_F(KeyTest, no_press) {
  // if there are no key presses detected, we shall be informed that this is the
  // case
  EXPECT_EQ(m_key.detected(), Key::KeyPress::no_press);

  // a tick shall have no effect on this
  m_key.tick();

  EXPECT_EQ(m_key.detected(), Key::KeyPress::no_press);
}

TEST_F(KeyTest, single_press) {
  // if there is a single keypress detected within the KEY_PRESS_PERIOD_MS, we shall
  // be informed that it is a single press
  EXPECT_CALL(m_hw, get_millis()).WillOnce(Return(0));
  m_key.press();

  // detected shall return no_press until the period has passed
  EXPECT_EQ(m_key.detected(), Key::KeyPress::no_press);

  EXPECT_CALL(m_hw, get_millis()).WillOnce(Return(100));
  m_key.tick();
  EXPECT_EQ(m_key.detected(), Key::KeyPress::no_press);

  EXPECT_CALL(m_hw, get_millis()).WillOnce(Return(200));
  m_key.tick();
  EXPECT_EQ(m_key.detected(), Key::KeyPress::no_press);

  EXPECT_CALL(m_hw, get_millis()).WillOnce(Return(300));
  m_key.tick();
  EXPECT_EQ(m_key.detected(), Key::KeyPress::no_press);

  EXPECT_CALL(m_hw, get_millis()).WillOnce(Return(KEY_PRESS_PERIOD_MS));
  m_key.tick();
  EXPECT_EQ(m_key.detected(), Key::KeyPress::single_press);
}

TEST_F(KeyTest, double_press) {
  // if we have two keypresses detected within the KEY_PRESS_PERIOD_MS, we shall
  // be informed that it is a double press
  EXPECT_CALL(m_hw, get_millis()).WillOnce(Return(0));
  m_key.press();
  m_key.press();

  // detected shall return no_press until the period has passed
  EXPECT_EQ(m_key.detected(), Key::KeyPress::no_press);

  EXPECT_CALL(m_hw, get_millis()).WillOnce(Return(100));
  m_key.tick();
  EXPECT_EQ(m_key.detected(), Key::KeyPress::no_press);

  EXPECT_CALL(m_hw, get_millis()).WillOnce(Return(200));
  m_key.tick();
  EXPECT_EQ(m_key.detected(), Key::KeyPress::no_press);

  EXPECT_CALL(m_hw, get_millis()).WillOnce(Return(300));
  m_key.tick();
  EXPECT_EQ(m_key.detected(), Key::KeyPress::no_press);

  EXPECT_CALL(m_hw, get_millis()).WillOnce(Return(KEY_PRESS_PERIOD_MS));
  m_key.tick();
  EXPECT_EQ(m_key.detected(), Key::KeyPress::double_press);
}
