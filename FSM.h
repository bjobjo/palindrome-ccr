// vim: ts=2 sw=2 expandtab:

#ifndef FSM_H
#define FSM_H

#include "HW.h"
#include "Key.h"
#include "PPO2Monitor.h"

#define LIBCALL_DEEP_SLEEP_SCHEDULER
#include <DeepSleepScheduler.h>

// The context class contains the information and functionality that is not
// state-specific, but should persist between states.
class Context {
  public:
    Context(HW &hw, PPO2Monitor &ppo2);

    // accessor function for battery voltage
    double vbat();

    // accessor function for hardware
    HW &hw();

    // accessor function for PPO2 monitor
    PPO2Monitor &ppo2();

  private:
    // hardware interface
    HW &m_hw;

    // PPO2 monitor
    PPO2Monitor &m_ppo2;
};

// additional state machine for the LED handling, created by the main state
// machine.
class LEDHandler : public Runnable {
  public:
    enum class State {
      read_ppO2,
      cell_failure,
      blink_alternate,
      blink_yellow,
      blink_red,
      blink_green,
      delay_until_next_read,
    };

    LEDHandler(Context &context);

    void run() override;

    State blink_cycle() const;

  private:
    Context &m_context;

    void led_toggle(Pins p);

    State m_blink_cycle{State::read_ppO2}, m_next_cycle{State::read_ppO2};
    unsigned int m_blink_count{0}, m_next_blink_count{0};
};

// main state machine for the key and display handling.
class FSM : public Runnable {
  public:
    enum class States {
      calibration,
      dive_average,
      dive_all_cells,
      shutdown,
    };

    enum class MenuOptions {
      exit,
      shutdown,
      calibrate,
      flip_screen,
    };

    FSM(HW &hw, PPO2Monitor &ppo2, Key &key, States initial = States::dive_average);
    virtual ~FSM() = default;

    // where the drawing magic happens
    virtual void run() override;

    // this is called from interrupt context
    void key_press();

    // accessor for the currently active state
    States state() const;

    // accessor for the LED handling
    LEDHandler &led();

  private:
    // function to handle the on-screen menu
    void handle_menu();
    
    // functions to draw the screen for each state + menu
    void draw_menu();
    void draw_calibration();
    void draw_dive_average();
    void draw_dive_all_cells();
    void draw_shutdown();

    // state transition accessor
    void transition(States next);

    // the currently active state
    States m_state{States::dive_average};

    // the common contextual information and functionality
    Context m_context;

    // key handling
    Key &m_key;

    // LED handling
    LEDHandler m_led;

    // the last time we transitioned to a new state
    unsigned long m_last_transition_ms{0};

    // is the menu visible?
    bool m_menu_visible{false};
    
    // the currently visible menu option
    MenuOptions m_option{MenuOptions::exit};

    // the last the we got key input
    unsigned long m_last_key_input_ms{0};
};

#endif // FSM_H
